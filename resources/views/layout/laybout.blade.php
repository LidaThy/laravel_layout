<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>view layout</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
</head>
<body>
    <nav>
        <div class="logo">
            <img src="{{URL('images/logo.png')}}">
        </div>
        <div class="nav">
            <ul>
                <li><a @class(['active'=>$page=='home']) href="{{URL('/')}}">home</a></li>
                <li><a @class(['active'=>$page=='about']) href="{{URL('/about')}}">About</a></li>
                <li><a @class(['active'=>$page=='contact']) href="{{URL('/contact')}}">contact</a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
        <div class="side_bar">
            <ul>
                <li><a @class(['active'=>$page=='category']) href="{{URL('/cat')}}">category</a></li>
                <li><a @class(['active'=>$page=='service']) href="{{URL('/service')}}">service</a></li>
                <li><a @class(['active'=>$page=='product']) href="{{URL('/product')}}">Product</a></li>
            </ul>
        </div>
        <div class="content">
            @yield('content')
            <div class="footer">
                <div class="contact">
                    <a href="https://mail.google.com/mail/u/0/#inbox"><img src="{{URL('images/mail.png')}}" class="mail"></a>
                    <a href="https://www.instagram.com/accounts/login/"><img src="{{URL('images/isg.png')}}" class="isg"></a>
                    <a href="https://www.facebook.com/profile.php?id=100034437217269"><img src="{{URL('images/facebook.png')}}" class="facebook"></a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
