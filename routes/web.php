<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $data['page']='home';
    return view('index', $data);
});
Route::get('/cat',function (){
    $data['page']='category';
    $data['data']=['lida','mey','keov'];
    return view('page.category',$data);
});

Route::get('/about',function (){
    $data['page']= 'about';
    return view('page.about', $data);
});

Route::get('/contact',function (){
    $data['page']='contact';
    return view('page.contact', $data);
});

Route::get('/service',function (){
    $data['page']='service';
    return view('page.service',$data);
});
Route::get('/product',function (){
    $data['page']='product';
    return view('page.product',$data);
});
